var words = [
  "accommodate | adapt, assist, house",
  "adverse | unfavorable, opposing",
  "alienate | estrange, antagonize find a place to rest",
  "anarchy | absence of government, chaos",
  "apposite | apt",
  "assent | agree",
  "badger | pester",
  "benefactor | patron",
  "bovine | cow-like",
  "canon | rule",
  "cessation | a stopping",
  "coalesce | combine",
  "comprise | consist of",
  "conscription | draft, enlistment",
  "convivial | sociable, festive",
  "cudgel | club",
  "decree | official order",
  "denouement | resolution",
  "dichotomy | a division into two parts",
  "dispatch | send",
  "dolorous | gloomy",
  "elaboration | detailed explanation",
  "endemic | peculiar to a particular region",
  "equable | even-tempered",
  "exacting | demanding, difficult",
  "extraneous | not essential",
  "ferment | turmoil",
  "forsake | abandon",
  "gingivitis | inflammation of the gums",
  "havoc | destruction, chaos",
  "idiosyncrasy | peculiarity",
  "implosion | bursting inward",
  "incognito | disguised",
  "infatuate | immature love",
  "insufferable | unbearable",
  "irascible | irritable",
  "lachrymose | tearful",
  "lissome | agile, supple",
  "mandrill | baboon",
  "mentor | teacher",
  "monolithic | large and uniform",
  "negligible | insignificant",
  "oblong | elliptical, oval",
  "orator | speaker",
  "panache | flamboyance",
  "pastoral | rustic",
  "perdition | damnation",
  "pettifogger | unscrupulous lawyer",
  "plentiful | abundant",
  "precept | principle, law",
  "prevaricate | lie",
  "promontory | headland, cape",
  "proxy | substitute, agent",
  "quadruped | four foot animal",
  "randy | vulgar",
  "recuperation | recovery",
  "render | deliver, provide",
  "resound | echo",
  "risque  | off-color, racy",
  "satanic | pertaining to the Devil",
  "sedentary | stationary, inactive",
  "shoal | reef",
  "sloth | laziness",
  "specious | false but plausible reasoning",
  "stevedore | longshoreman",
  "subvert | undermine",
  "syllabicate | divide into syllables",
  "tempo | speed",
  "tryst | meeting, rendezvous",
  "unstinting | generous",
  "viaduct | waterway",
  "wary | guarded, cautious"
];

var definitions = [];

const delimeter = "|";

// Splits array
for (i = 0; i < words.length; i++) 
{
  var splitWords = words[i].replace(delimeter, "<span style='font-weight: normal'>means</span>");
  definitions[i] = splitWords;
}

var myRandom = Math.floor(Math.random() * words.length);

// logging
console.log(myRandom);
console.log(definitions[myRandom]);


// // PAGE WRITTING

// first definition
var htmlDef1 = "<p> Word: <b>" + definitions[myRandom] + "</b></p>";
document.write(htmlDef1);

// second definition
myRandom = Math.floor(Math.random() * words.length);
var htmlDef2 = "<p> Word: <b>" + definitions[myRandom] + "</b></p>";
document.write(htmlDef2);

//third definition
myRandom = Math.floor(Math.random() * words.length);
var htmlDef3 = "<p> Word: <b>" + definitions[myRandom] + "</b></p>";
document.write(htmlDef3);